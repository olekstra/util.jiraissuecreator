﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Util.JiraIssueCreator
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false)
                .AddUserSecrets<Program>()
                .Build();

            var services = new ServiceCollection()
                .AddSingleton<IConfiguration>(config)
                .AddHttpClient()
                .BuildServiceProvider();

            var httpClientFactory = services.GetRequiredService<IHttpClientFactory>();
            var httpClient = httpClientFactory.CreateClient();
            httpClient.BaseAddress = new Uri(new Uri(config["JiraServer"]), "/rest/api/2/");

            var authBytes = Encoding.UTF8.GetBytes(config["JiraUser"] + ":" + config["JiraPassword"]);
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(authBytes));

            var projectKey = config["ProjectKey"];
            var issueType = config["IssueType"];
            var userName = config["ReporterAndAssignee"];

            using var outputFile = File.CreateText("out.csv");

            await foreach(var data in ReadFileAsync())
            {
                var title = data.code + ": " + data.address;

                var issueData = new
                {
                    fields = new
                    {
                        project = new { key = projectKey },
                        issuetype = new { name = issueType },
                        parent = new { key = projectKey + "-" + data.parentId },
                        summary = title,
                        description = title,
                        reporter = new { id = userName },
                        assignee = new { id = userName },
                    }
                };

                var requestText = JsonConvert.SerializeObject(issueData);

                using var req = new HttpRequestMessage(HttpMethod.Post, "issue");
                req.Content = new StringContent(requestText, Encoding.UTF8, "application/json");

                using var resp = await httpClient.SendAsync(req);
                var respContent = await resp.Content.ReadAsStringAsync();

                if (resp.StatusCode != System.Net.HttpStatusCode.Created)
                {
                    var msg = string.Format(
                        "Error creating issue in Jira: HTTP status {0}, response \n{1} ",
                        resp.StatusCode,
                        respContent);
                    throw new Exception(msg);
                }

                dynamic respData = JsonConvert.DeserializeObject(respContent);
                var issueId = (string)respData.key.Value;

                Console.WriteLine("Jira issue created: {0}", issueId);
                await outputFile.WriteLineAsync($"{data.code},{issueId}");
            }

            Console.WriteLine("DONE");
            Console.ReadLine();
        }

        public static async IAsyncEnumerable<(int parentId, string code, string address)> ReadFileAsync()
        {
            using var file = File.OpenText("data.csv");
            var lineCount = 0;

            while (!file.EndOfStream)
            {
                var line = await file.ReadLineAsync();
                lineCount++;

                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                if (line.StartsWith('#'))
                {
                    continue;
                }

                var fields = line.Split('\t', 3);
                if (fields.Length != 3)
                {
                    Console.WriteLine($"Bad line #{lineCount} (need 3 fields) (skipped): {line}");
                    continue;
                }

                var parentId = int.Parse(fields[0]);
                var code = fields[1].Replace('\t', ' ');
                var text = fields[2].Replace('\t', ' ');
                yield return (parentId, code, text);
            }
        }
    }
}
